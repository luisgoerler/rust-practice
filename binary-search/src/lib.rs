// returns the index, not the found item itself
pub fn search<T: PartialEq + PartialOrd>(items: Vec<T>, target: T) -> Option<u32> {
    let mut left: u32 = 0;
    let mut right: u32 = items.len() as u32 - 1;
    while left <= right {
        let mid = left + (right - left)/2;
        if items[mid as usize] == target {
            return Some(mid);
        } else if items[mid as usize] > target {
            right = mid - 1;
        } else {
            left = mid + 1;
        }
    }

    None
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn not_findable() {
        assert_eq!(search(vec![-1, 1, 2, 3, 4, 5], 7), None);
        assert_eq!(search(vec![-3, 0, 2, 4, 7, 11, 12, 17], 6), None);
    }

    #[test]
    fn at_middle() {
        assert_eq!(search(vec![-1, 0, 1, 2, 3, 4, 5], 2), Some(3));
    }

    #[test]
    fn in_left_half() {
        assert_eq!(search(vec![-3, 4, 7, 11, 12, 17], 4), Some(1));
    }

    #[test]
    fn in_right_half() {
        assert_eq!(search(vec![-12, -3, 5, 11, 12, 17], 12), Some(4));
    }

    #[test]
    fn at_bound() {
        assert_eq!(search(vec![-1, 2, 4, 7, 11, 12, 17], 17), Some(6));
        assert_eq!(search(vec![-2, 2, 4, 7, 11, 12, 17], -2), Some(0));
    }
}

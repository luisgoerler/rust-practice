// is a max-heap by default
pub struct BinaryHeap<T> {
    items: Vec<T>,
}

impl<T: Clone + PartialEq + PartialOrd> BinaryHeap<T> {

    pub fn new() -> BinaryHeap<T> {
        BinaryHeap {
            items: Vec::new(),
        }
    }

    pub fn with_capacity(capacity: usize) -> BinaryHeap<T> {
        BinaryHeap {
            items: Vec::with_capacity(capacity),
        }
    }

    pub fn push(&mut self, item: T) {
        self.items.push(item);
        self.heapify_up();
    }

    pub fn pop(&mut self) -> Option<T> {
        if self.items.len() == 0 {
            None
        } else {
            let result = self.items.swap_remove(0);
            self.heapify_down();
            Some(result)
        }
    }

    fn heapify_up(&mut self) {
        let mut index = self.items.len() - 1;
        loop {
            if let Some(parent_index) = self.get_parent_index(index) {
                if self.items[parent_index] < self.items[index] {
                    self.swap(index, parent_index);
                    index = parent_index;
                } else {
                    break;
                }
            } else {
                // if the index is zero (root => no parent)
                break;
            }
        }
    }

    fn heapify_down(&mut self) {
        let mut index = 0;
        loop {
            let (index0, index1) = self.get_child_indices(index);

            if let Some(index0) = index0 {
                if let Some(index1) = index1 {
                    if self.items[index1] > self.items[index] 
                            && self.items[index1] > self.items[index0] {
                        self.swap(index1, index);
                        index = index1;
                        continue;
                    }
                }

                if self.items[index0] > self.items[index] {
                    self.swap(index0, index);
                    index = index0;
                } else {
                    break;
                }

            } else {
                break;
            }
        }
    }

    fn swap(&mut self, index0: usize, index1: usize) {
        let val0 = self.items[index0].clone();
        let val1 = self.items[index1].clone();
        self.items[index0] = val1;
        self.items[index1] = val0;
    }

    fn get_parent_index(&self, index: usize) -> Option<usize> {
        if index != 0 {
            Some((index - 1)/2)
        } else {
            None
        }
    }

    fn get_child_indices(&self, index: usize) -> (Option<usize>, Option<usize>) {
        let left_child_index = index * 2 + 1;
        if left_child_index >= self.items.len() {
            (None, None)
        } else if left_child_index + 1 >= self.items.len() {
            (Some(left_child_index), None)
        } else {
            (Some(left_child_index), Some(left_child_index + 1))
        }
    }

}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn push() {
        let mut heap = build_test_heap();
        heap.push(7);
        heap.push(11);
        heap.push(3);
        assert_eq!(heap.items[0], 11);
    }

    #[test]
    fn pop() {
        let mut heap: BinaryHeap<i32> = BinaryHeap::new();
        assert_eq!(heap.pop(), None);
        let mut heap = build_test_heap();
        assert_eq!(heap.pop(), Some(9));
    }

    #[test]
    fn heapify_up() {
        let mut heap = build_test_heap();
        heap.items.push(7);
        assert_eq!(heap.items[1], 5);
        assert_eq!(heap.items[3], -1);
        assert_eq!(heap.items[7], 7);
        heap.heapify_up();
        assert_eq!(heap.items[1], 7);
        assert_eq!(heap.items[3], 5);
        assert_eq!(heap.items[7], -1);
    }

    #[test]
    fn heapify_down() {
        let mut heap = build_test_heap();
        heap.items.push(-10);
        heap.items.swap_remove(0);

        assert_eq!(heap.items[0], -10);
        assert_eq!(heap.items[2], 6);
        assert_eq!(heap.items[6], 6);
        heap.heapify_down();
        assert_eq!(heap.items[0], 6);
        assert_eq!(heap.items[2], 6);
        assert_eq!(heap.items[6], -10);
    }

    #[test]
    fn swap() {
        let mut heap: BinaryHeap<i32> = BinaryHeap::new();
        heap.items.push(3);
        heap.items.push(4);
        assert_eq!(heap.items[0], 3);
        assert_eq!(heap.items[1], 4);
        heap.swap(0, 1);
        assert_eq!(heap.items[0], 4);
        assert_eq!(heap.items[1], 3);
    }

    #[test]
    fn get_parent_index() {
        let heap: BinaryHeap<i32> = BinaryHeap::new();
        assert_eq!(heap.get_parent_index(0), None);
        let heap: BinaryHeap<i32> = BinaryHeap::new();
        assert_eq!(heap.get_parent_index(8), Some(3));
        assert_eq!(heap.get_parent_index(7), Some(3));
    }

    #[test]
    fn get_child_indices() {
        let mut heap = build_test_heap();
        heap.items.push(-10);
        assert_eq!(heap.get_child_indices(7), (None, None));
        assert_eq!(heap.get_child_indices(3), (Some(7), None));
        assert_eq!(heap.get_child_indices(0), (Some(1), Some(2)));
    }

    fn build_test_heap() -> BinaryHeap<i32> {
        let mut heap: BinaryHeap<i32> = BinaryHeap::with_capacity(7);
        heap.items.push(9);
        heap.items.push(5);
        heap.items.push(6);
        heap.items.push(-1);
        heap.items.push(2);
        heap.items.push(4);
        heap.items.push(6);

        heap
    }
}

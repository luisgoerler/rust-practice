use std::{env, fmt, process, error::Error};
use huffman_coding;
use huffman_coding::Direction;


fn main() {
    if let Err(e) = run() {
        eprintln!("{}", e);
        process::exit(1);
    }
}


fn run() -> Result<(), Box<dyn Error>> {
    let mut args = env::args();
    // skipping the first argument (name of the program)
    args.next();

    let direction = match args.next() {
        Some(d) => Direction::new(d.as_str())?,
        None => return Err(Box::new(CustomError::msg("No direction was given"))),
    };

    let filename = match args.next() {
        Some(s) => s,
        None => return Err(Box::new(CustomError::msg("No filename was given"))),
    };

    match direction {
        Direction::Encoding => huffman_coding::encode(filename.as_str())?,
        Direction::Decoding => huffman_coding::decode(filename.as_str())?,
    }

    Ok(())
}

#[derive(Debug)]
struct CustomError {
    msg: &'static str,
}

impl CustomError {
    fn msg(s: &'static str) -> CustomError {
        CustomError { msg: s }
    }
}

impl fmt::Display for CustomError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.msg)
    }
}

impl Error for CustomError {}

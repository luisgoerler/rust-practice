use bitbit::writer::BitWriter;
use bitbit::reader::{BitReader, MSB};
use serde::{Deserialize, Serialize};
use serde_json;
use std::collections::{BinaryHeap, HashMap};
use std::cmp::Ordering;
use std::hash::Hash;
use std::error::Error;
use std::fs;
use std::fs::File;


pub enum Direction {
    Encoding,
    Decoding,
}

impl Direction {
    pub fn new(s: &str) -> Result<Direction, &'static str> {
        match s {
            "e" => Ok(Direction::Encoding),
            "d" => Ok(Direction::Decoding),
            _ => Err("Invalid direction, please use \"e\" for encoding or \"d\" for decoding"),
        }
    }
}

// has to be public for benchmarks
#[derive (Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum CharNode {
    End(char),
    Connector(Box<[CharNode; 2]>),
}

impl CharNode {
    fn find_path_wrapped(&self, target: char) -> Result<Vec<bool>, &str> {
        match self.find_path(target, &Vec::new()) {
            Some(path) => Ok(path),
            None => Err("No path could be found"),
        }
    }

    fn find_path(&self, target: char, tracks: &Vec<bool>) -> Option<Vec<bool>> {
        match self {
            CharNode::End(c) => if *c == target {
                Some(tracks.clone())
            } else {
                None
            },
            CharNode::Connector(children) => {
                for i in [false, true] {
                    let mut new_tracks = tracks.clone();
                    new_tracks.push(i);
                    match children[usize::from(i)].find_path(target, &new_tracks) {
                        None => (),
                        Some(path) => return Some(path),
                    }
                }

                None
            },
        }
    }

    fn get_char<'a, T: Iterator<Item = bool>>(&self, path: &mut T) -> Result<char, &str> {
        let mut current_node = self;
        loop {
            match current_node {
                CharNode::End(c) => return Ok(*c),
                CharNode::Connector(children) => {
                    if let Some(index) = path.next() {
                        current_node = &children[usize::from(index)];
                    } else {
                        return Err("The given path is empty or not complete");
                    }
                },
            }
        }
    }
}

#[derive (PartialEq, Eq)]
struct CharInfo {
    char_node: CharNode,
    count: u32,
}

// makes BinaryHeap a min-heap
impl CharInfo {
    fn inverse_cmp(&self, other: &Self) -> Ordering {
        if self.count > other.count {
            Ordering::Less
        } else if self.count < other.count {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    }
}

impl PartialOrd for CharInfo {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.inverse_cmp(other))
    }
}

impl Ord for CharInfo {
    fn cmp(&self, other: &Self) -> Ordering {
        self.inverse_cmp(other)
    }
}

#[derive (Debug, PartialEq)]
struct CombinedCharInfo {
    char_nodes: [CharNode; 2],
    count: u32,
}

fn count_char_occurrences(filename: &str) -> Result<HashMap<CharNode, u32>, Box<dyn Error>> {
    let content = fs::read_to_string(filename)?;
    let mut char_nodes_to_counts = HashMap::new();

    for line in content.lines() {
        for c in line.chars() {
            let count = char_nodes_to_counts.entry(CharNode::End(c)).or_insert(0);
            *count += 1;
        }

        let count = char_nodes_to_counts.entry(CharNode::End('\n')).or_insert(0);
        *count += 1;
    }

    Ok(char_nodes_to_counts)
}

fn pop_lowest_counts(char_node_counts: &mut BinaryHeap<CharInfo>) -> Option<CombinedCharInfo> {
    if let Some(c_info0) = char_node_counts.pop() {
        if let Some(c_info1) = char_node_counts.pop() {
            let info = CombinedCharInfo {
                char_nodes: [c_info0.char_node, c_info1.char_node],
                count: c_info0.count + c_info1.count,
            };
            Some(info)
        } else {
            char_node_counts.push(c_info0);
            None
        }
    } else {
        None
    }
}

// has to be public for benchmarks
pub fn grow_huffman_tree(char_nodes_to_counts: HashMap<CharNode, u32>) -> Result<CharNode, &'static str> {
    if char_nodes_to_counts.is_empty() {
        return Err("The given map contains no entries");
    }

    let mut char_node_counts = BinaryHeap::with_capacity(char_nodes_to_counts.len());
    for (c, count) in char_nodes_to_counts.into_iter() {
        char_node_counts.push(CharInfo { char_node: c, count: count });
    }

    loop {
        match pop_lowest_counts(&mut char_node_counts) {
            Some(c_info) => {
                let mut children = Vec::new();
                for node in c_info.char_nodes {
                    children.push(node);
                }
                let connector = CharNode::Connector(Box::new([children.remove(0), children.remove(0)]));
                char_node_counts.push(CharInfo { char_node: connector, count: c_info.count });
            },
            None => break,
        }
    }

    match char_node_counts.pop() {
        Some(rootnode_info) => Ok(rootnode_info.char_node),
        // the map of counts that is provided is checked to be non-empty at the beginning
        None => panic!("This never happens"),
    }
}

pub fn encode(filename: &str) -> Result<(), Box<dyn Error>> {
    let char_nodes_to_counts = count_char_occurrences(filename)?;

    let rootnode = grow_huffman_tree(char_nodes_to_counts)?;

    let content_src = fs::read_to_string(filename)?;
    let mut content_target = Vec::new();

    for line in content_src.lines() {
        for c in line.chars() {
            content_target.append(&mut rootnode.find_path_wrapped(c)?);
        }
        content_target.append(&mut rootnode.find_path_wrapped('\n')?);
    }

    let mut padding_count = 8 - (content_target.len() % 8);
    if padding_count == 8 {
        padding_count = 0;
    }
    let filename_base = filename.split(".").collect::<Vec<&str>>()[0].to_string();
    let mut padding_filename = filename_base.clone();
    padding_filename.push_str("_padding_count.json");
    fs::write(padding_filename.as_str(), serde_json::to_string(&padding_count)?.as_str())?;

    let mut content_target_filename = filename_base.clone();
    content_target_filename.push_str("_compressed");
    let file = File::create(content_target_filename.as_str())?;
    let mut bw = BitWriter::new(file);
    for bit in content_target {
        bw.write_bit(bit)?;
    }
    bw.pad_to_byte()?;

    let mut tree_filename = filename_base.clone();
    tree_filename.push_str("_tree.json");
    fs::write(tree_filename.as_str(), serde_json::to_string(&rootnode)?.as_str())?;
    Ok(())
}

pub fn decode(filename: &str) -> Result<(), Box<dyn Error>> {
    let filename_base = filename.split(".").collect::<Vec<&str>>()[0].to_string();
    let mut tree_filename = filename_base.clone();
    tree_filename.push_str("_tree.json");
    let rootnode: CharNode = serde_json::from_str(fs::read_to_string(tree_filename)?.as_str())?;
    let mut bits = Vec::new();
    let mut src_filename = filename_base.clone();
    src_filename.push_str("_compressed");
    let mut br: BitReader<_, MSB> = BitReader::new(File::open(src_filename)?);
    loop {
        match br.read_bit() {
            Ok(b) => bits.push(b),
            Err(_) => break,
        }
    }

    let mut padding_filename = filename_base.clone();
    padding_filename.push_str("_padding_count.json");
    let padding_count: usize = serde_json::from_str(fs::read_to_string(padding_filename)?.as_str())?;
    bits.truncate(bits.len() - padding_count);

    let mut decompressed_content = String::new();
    let mut path = bits.into_iter();
    loop {
        match rootnode.get_char(&mut path) {
            Ok(c) => decompressed_content.push(c),
            Err(_) => break,
        }
    }

    let mut target_filename = filename_base.clone();
    target_filename.push_str("_decompressed.txt");
    fs::write(target_filename.as_str(), decompressed_content.as_str())?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    fn grow_test_tree() -> CharNode {
        let child0 = CharNode::End('a');
        let child1 = CharNode::End('b');
        let child2 = CharNode::End('c');
        let child3 = CharNode::End('d');
        let child4 = CharNode::End('e');
        let parent0 = CharNode::Connector(Box::new([child0, child1]));
        let parent1 = CharNode::Connector(Box::new([child3, child4]));
        let parent2 = CharNode::Connector(Box::new([parent0, child2]));
        CharNode::Connector(Box::new([parent2, parent1]))
    }

    #[test]
    fn find_path_wrapped_valid() {
        let root = grow_test_tree();
        assert_eq!(root.find_path_wrapped('b'), Ok(vec![false, false, true]));
    }

    #[test]
    fn find_path_wrapped_invalid() {
        let child0 = CharNode::End('a');
        let child1 = CharNode::End('b');
        let parent = CharNode::Connector(Box::new([child0, child1]));
        assert_eq!(parent.find_path_wrapped('x'), Err("No path could be found"));
    }

    #[test]
    fn get_char_incomplete_path() {
        let root = grow_test_tree();
        let path = vec!(false, false);
        assert_eq!(root.get_char(&mut path.into_iter()), Err("The given path is empty or not complete"));
    }

    #[test]
    fn get_char_complete_path() {
        let root = grow_test_tree();
        let path = vec!(false, false, true);
        assert_eq!(root.get_char(&mut path.into_iter()), Ok('b'));
    }

    #[test]
    fn get_chars() {
        let root = grow_test_tree();
        let mut path = vec!(false, false, true, true, true).into_iter();
        assert_eq!(root.get_char(&mut path), Ok('b'));
        assert_eq!(root.get_char(&mut path), Ok('e'));
    }

    #[test]
    fn pop_lowest_counts_empty() {
        let mut char_node_counts = BinaryHeap::new();
        assert_eq!(pop_lowest_counts(&mut char_node_counts), None);
    }

    #[test]
    fn pop_lowest_counts_one_entry() {
        let mut char_node_counts = BinaryHeap::new();
        char_node_counts.push(CharInfo { char_node: CharNode::End('a'), count: 1 });
        assert_eq!(pop_lowest_counts(&mut char_node_counts), None);
        assert_eq!(char_node_counts.len(), 1);
    }

    #[test]
    fn pop_lowest_counts_multiple_entries() {
        let mut char_node_counts = BinaryHeap::new();
        char_node_counts.push(CharInfo { char_node: CharNode::End('a'), count: 1 });
        char_node_counts.push(CharInfo { char_node: CharNode::End('b'), count: 2 });
        let expected_result = CombinedCharInfo {
            char_nodes: [CharNode::End('a'), CharNode::End('b')],
            count: 3,
        };
        assert_eq!(pop_lowest_counts(&mut char_node_counts), Some(expected_result));
        assert_eq!(char_node_counts.len(), 0);
    }

    #[test]
    fn grow_huffman_tree_non_empty_counts() {
        let mut char_nodes_to_counts = HashMap::new();
        char_nodes_to_counts.insert(CharNode::End('a'), 1);
        char_nodes_to_counts.insert(CharNode::End('b'), 2);
        char_nodes_to_counts.insert(CharNode::End('c'), 7);
        char_nodes_to_counts.insert(CharNode::End('d'), 8);
        char_nodes_to_counts.insert(CharNode::End('e'), 9);
        let root = grow_test_tree();
        assert_eq!(grow_huffman_tree(char_nodes_to_counts).unwrap(), root);
    }

    #[test]
    fn grow_huffman_tree_empty_counts() {
        let char_nodes_to_counts = HashMap::new();
        assert_eq!(grow_huffman_tree(char_nodes_to_counts), Err("The given map contains no entries"));
    }
}

use criterion::{criterion_group, criterion_main, Criterion, BenchmarkId};
use huffman_coding;
use huffman_coding::CharNode;
use std::collections::HashMap;


fn create_test_map() -> HashMap<CharNode, u32> {
    let mut char_nodes = vec![];
    for i in 33..127 {
        char_nodes.push(CharNode::End(char::from_u32(i as u32).unwrap()));
    }
    let mut counts = vec![];
    for _ in 0..6 {
        counts.push(3);
        counts.push(1);
        counts.push(120);
        counts.push(178);
        counts.push(13);
        counts.push(1);
        counts.push(9);
        counts.push(5);
        counts.push(33);
        counts.push(75);
        counts.push(52);
        counts.push(53);
        counts.push(334);
        counts.push(599);
        counts.push(22);
        counts.push(334);
    }
    counts.push(79);

    let char_nodes_to_counts: HashMap<_, _> = char_nodes.into_iter().zip(counts.into_iter()).collect();
    char_nodes_to_counts
}


fn grow_tree_benchmark(c: &mut Criterion) {
    let test_map = create_test_map();
    c.bench_with_input(BenchmarkId::new("grow tree", ""), &test_map,
        |b, test_map| b.iter(|| {
            huffman_coding::grow_huffman_tree(test_map.clone()).unwrap();
            ()
        }));
}


criterion_group!(benches, grow_tree_benchmark);
criterion_main!(benches);

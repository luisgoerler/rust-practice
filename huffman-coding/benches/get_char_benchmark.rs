use criterion::{criterion_group, criterion_main, Criterion, BenchmarkId};


enum CharNode {
    End(char),
    Connector(Box<[CharNode; 2]>),
}

impl CharNode {
    fn get_char_iter<'a, T: Iterator<Item = bool>>(&self, path: &mut T) -> Result<char, &str> {
        let mut current_node = self;
        loop {
            match current_node {
                CharNode::End(c) => return Ok(*c),
                CharNode::Connector(children) => {
                    if let Some(index) = path.next() {
                        current_node = &children[usize::from(index)];
                    } else {
                        return Err("The given path is empty or not complete");
                    }
                },
            }
        }
    }

    fn get_char_slice<'a>(&self, mut path: &'a[bool]) -> Result<(char, &'a[bool]), &str> {
        let mut current_node = self;
        loop {
            match current_node {
                CharNode::End(c) => return Ok((*c, path)),
                CharNode::Connector(children) => {
                    if let Some((index, new_path)) = path.split_first() {
                        current_node = &children[usize::from(*index)];
                        path = new_path;
                    } else {
                        return Err("The given path is not complete");
                    }
                },
            }
        }
    }

    fn get_char_vec(&self, path: &mut Vec<bool>) -> Result<char, &str> {
        let mut current_node = self;
        loop {
            match current_node {
                CharNode::End(c) => return Ok(*c),
                CharNode::Connector(children) => {
                    if path.len() == 0 {
                        return Err("The given path is not complete");
                    }
                    current_node = &children[usize::from(path.remove(0))];
                },
            }
        }
    }
}

fn grow_test_tree() -> CharNode {
    let child0 = CharNode::End('a');
    let child1 = CharNode::End('b');
    let child2 = CharNode::End('c');
    let child3 = CharNode::End('d');
    let child4 = CharNode::End('e');
    let child5 = CharNode::End('f');
    let child6 = CharNode::End('g');
    let parent0 = CharNode::Connector(Box::new([child0, child1]));
    let parent1 = CharNode::Connector(Box::new([child3, child4]));
    let parent2 = CharNode::Connector(Box::new([parent0, child2]));
    let parent3 = CharNode::Connector(Box::new([parent2, parent1]));
    let parent4 = CharNode::Connector(Box::new([parent3, child5]));
    CharNode::Connector(Box::new([parent4, child6]))
}


fn get_char_benchmark(c: &mut Criterion) {
    let root = grow_test_tree();
    let path = vec!(false, false, false, false, true);
    let mut group = c.benchmark_group("get char");
    group.bench_with_input(BenchmarkId::new("iter", ""), &path,
        |b, path| b.iter(|| root.get_char_iter(&mut path.clone().into_iter())));
    group.bench_with_input(BenchmarkId::new("slice", ""), &path,
        |b, path| b.iter(|| {
            let cloned_path = path.clone();
            root.get_char_slice(&cloned_path[..]).unwrap();
            ()
        }));
    group.bench_with_input(BenchmarkId::new("vec", ""), &path,
        |b, path| b.iter(|| root.get_char_vec(&mut path.clone())));
    group.finish();
}


criterion_group!(benches, get_char_benchmark);
criterion_main!(benches);

use std::time::{SystemTime, UNIX_EPOCH};


pub struct RNG {
    a: u32,
    c: u32,
    m: u32,
    seed: u32,
}

impl RNG {

    pub fn seed() -> u32 {
        SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_nanos() as u32 % 10
    }

    // uses the same parameters as the ZX81
    pub fn new(seed: u32) -> RNG {
        RNG {
            a: 75,
            c: 74,
            m: 2_u32.pow(16) + 1,
            seed: seed,
        }
    }

    pub fn psrand(&mut self) -> u32 {
        self.seed = (self.a * self.seed + self.c) % self.m;
        self.seed
    }

    // the boundary is inclusive on the left, exclusive on the right
    pub fn psrand_in_bounds(&mut self, boundaries: (u32, u32)) -> u32 {
        if boundaries.1 - boundaries.0 <= 1 {
            panic!("The upper boundary must be higher than the lower boundary by at least 2");
        } else if boundaries.1 - boundaries.0 > self.m {
            panic!("The specified range is too big");
        }
        self.psrand() % (boundaries.1 - boundaries.0) + boundaries.0
    }

}

mod plotting;
mod psrand;


use plotting::BitMap;
use plotting::WHITE;
use psrand::RNG;
use std::error::Error;


const MAP_SIDE_LENGTH: u32 = 1000;
const CHAOS_POINTS_COUNT: u32 = 50000;


fn go_halfway(src: f64, target: f64) -> f64 {
    (src + target)/2.0
}

fn calc_chaos_point(point: (f64, f64), corners: [(f64, f64); 3], chaos_source: &mut RNG) -> (f64, f64) {
    let rand_index = chaos_source.psrand_in_bounds((0, 3)) as usize;
    (go_halfway(point.0, corners[rand_index].0), go_halfway(point.1, corners[rand_index].1))
}

fn create_chaos_points(count: u32) -> Vec<(f64, f64)> {
    let mut chaos_src = RNG::new(RNG::seed());
    let corners = [(0.0, 0.0), (1.0, 0.0), (0.5, 1.0)];
    let mut points = Vec::new();

    let mut last_point = (0.5, 0.5);
    for _i in 0..count {
        let new_point = calc_chaos_point(last_point, corners, &mut chaos_src);
        points.push(new_point);
        last_point = new_point;
    }

    points
}

fn scale_to_range(src: &f64, boundaries: (u32, u32)) -> u32 {
    if boundaries.1 - boundaries.0 < 1 {
        panic!("The upper boundary must be higher than the lower boundary by at least 1");
    }
    let range_size = boundaries.1 - boundaries.0;
    (src * range_size as f64 + boundaries.0 as f64) as u32
}

fn transform_points(points: &Vec<(f64, f64)>, boundaries: (u32, u32)) -> Vec<(u32, u32)> {
    points.iter().map(|(a, b)| (scale_to_range(a, boundaries), scale_to_range(b, boundaries))).collect()
}

pub fn plot_chaos_points(file_name: &str) -> Result<(), Box<dyn Error>> {
    let points = create_chaos_points(CHAOS_POINTS_COUNT);
    let points = transform_points(&points, (0, MAP_SIDE_LENGTH));
    let mut bm = BitMap::new(MAP_SIDE_LENGTH, MAP_SIDE_LENGTH);
    for point in points.iter() {
        bm.set_pixel(point.0, point.1, WHITE)?;
    }

    bm.save(file_name)?;
    Ok(())
}

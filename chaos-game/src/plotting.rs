use byteorder::{LittleEndian, WriteBytesExt};
use std::error::Error;
use std::fs::File;
use std::io::Write;
use std::path::Path;


const COLOR_DEPTH: u32 = 24;
const BMP_SIGNATURE: [u8; 2] = [66, 77];


struct BitMapSizeInfo {
    size: u32,
    row_padding: u32,
}

fn calc_unpadded_rowsize(width: u32) -> u32 {
    ((width * COLOR_DEPTH) as f32/8_f32) as u32
}

fn calc_row_padding(bytes_amount: u32) -> u32 {
    let mut padding_amount = 4 - bytes_amount % 4;
    if padding_amount == 4 {
        padding_amount = 0;
    }

    padding_amount
}

fn calc_map_size(width: u32, height: u32) -> BitMapSizeInfo {
    let bytes_amount = calc_unpadded_rowsize(width);
    let padding = calc_row_padding(bytes_amount);
    BitMapSizeInfo {
        size: (bytes_amount + padding) * height,
        row_padding: padding,
    }
}

pub struct Pixel {
    r: u8,
    g: u8,
    b: u8,
}

impl Pixel {

    pub fn new(r: u8, g: u8, b: u8) -> Pixel {
        Pixel { r, g, b, }
    }

}

pub const WHITE: Pixel = Pixel {
    r: 255,
    g: 255,
    b: 255,
};

struct BmpHeader {
    file_size: u32,
    creator1: u16,
    creator2: u16,
    pixel_offset: u32,
}

impl BmpHeader {

    fn new(data_size: u32) -> BmpHeader {
        // file signature: 2 bytes, BmpHeader: 12 bytes, BmpDibHeader: 40 bytes
        let header_size = 54 as u32;
        BmpHeader {
            file_size: header_size + data_size,
            creator1: 0,
            creator2: 0,
            pixel_offset: header_size,
        }
    }

}

struct BmpDibHeader {
    header_size: u32,
    width: i32,
    height: i32,
    num_planes: u16,
    bits_per_pixel: u16,
    compress_type: u32,
    data_size: u32,
    hres: i32,
    vres: i32,
    num_colors: u32,
    num_imp_colors: u32,
}

impl BmpDibHeader {

    fn new(width: u32, height: u32, data_size: u32) -> BmpDibHeader {
        BmpDibHeader {
            header_size: 40,
            width: width as i32,
            height: height as i32,
            num_planes: 1,
            bits_per_pixel: COLOR_DEPTH as u16,
            compress_type: 0,
            data_size,
            hres: 1000,
            vres: 1000,
            num_colors: 0,
            num_imp_colors: 0,
        }
    }

}

pub struct BitMap {
    header: BmpHeader,
    dib_header: BmpDibHeader,
    width: u32,
    height: u32,
    row_padding: u32,
    data: Vec<Pixel>,
}

impl BitMap {

    // the returned BitMap is completely black by default
    pub fn new(width: u32, height: u32) -> BitMap {
        let mut pixels = Vec::with_capacity((width * height) as usize);
        for _ in 0..width * height {
            pixels.push(Pixel::new(0, 0, 0));
        }

        let size_info = calc_map_size(width, height);

        BitMap {
            header: BmpHeader::new(size_info.size),
            dib_header: BmpDibHeader::new(width, height, size_info.size),
            width,
            height,
            row_padding: size_info.row_padding,
            data: pixels,
        }
    }

    pub fn set_pixel(&mut self, x: u32, y: u32, val: Pixel) -> Result<(), &str> {
        if x >= self.width {
            Err("The given x is too big")
        } else if y >= self.height {
            Err("The given y is too big")
        } else {
            self.data[(y * self.width + x) as usize] = val;
            Ok(())
        }
    }

    fn write_header(&self, target: &mut Vec<u8>) -> Result<(), Box<dyn Error>> {
        let header = &self.header;
        let dib_header = &self.dib_header;

        Write::write(target, &BMP_SIGNATURE)?;

        target.write_u32::<LittleEndian>(header.file_size)?;
        target.write_u16::<LittleEndian>(header.creator1)?;
        target.write_u16::<LittleEndian>(header.creator2)?;
        target.write_u32::<LittleEndian>(header.pixel_offset)?;

        target.write_u32::<LittleEndian>(dib_header.header_size)?;
        target.write_i32::<LittleEndian>(dib_header.width)?;
        target.write_i32::<LittleEndian>(dib_header.height)?;
        target.write_u16::<LittleEndian>(dib_header.num_planes)?;
        target.write_u16::<LittleEndian>(dib_header.bits_per_pixel)?;
        target.write_u32::<LittleEndian>(dib_header.compress_type)?;
        target.write_u32::<LittleEndian>(dib_header.data_size)?;
        target.write_i32::<LittleEndian>(dib_header.hres)?;
        target.write_i32::<LittleEndian>(dib_header.vres)?;
        target.write_u32::<LittleEndian>(dib_header.num_colors)?;
        target.write_u32::<LittleEndian>(dib_header.num_imp_colors)?;

        Ok(())
    }

    fn write_data(&self, target: &mut Vec<u8>) -> Result<(), Box<dyn Error>> {
        let padding_size = self.row_padding;
        let mut padding: Vec<u8> = Vec::new();
        for _ in 0..padding_size {
            padding.push(0);
            padding.push(0);
        }

        for y in 0..self.height {
            for x in 0..self.width {
                let index = (y * self.width + x) as usize;
                let pixel = &self.data[index];
                Write::write(target, &[pixel.b, pixel.g, pixel.r])?;
            }
            Write::write(target, &padding)?;
        }

        Ok(())
    }

    fn encode(&self) -> Result<Vec<u8>, Box<dyn Error>> {
        let mut result = Vec::with_capacity(self.header.file_size as usize);

        self.write_header(&mut result)?;
        self.write_data(&mut result)?;
        Ok(result)
    }

    pub fn save<P: AsRef<Path>>(&self, path: P) -> Result<(), Box<dyn Error>> {
        let mut bmp_file = File::create(path)?;
        bmp_file.write(&self.encode()?)?;
        Ok(())
    }

}

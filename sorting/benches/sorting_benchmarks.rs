use criterion::{criterion_group, criterion_main, Criterion, BenchmarkId};
use rand::Rng;
use sorting::{bubble_sort, insertion_sort};


fn sorting_benchmarks(c: &mut Criterion) {
    let mut rng = rand::thread_rng();
    let test_items: Vec<i32> = (0..1000).map(|_| rng.gen_range(0..100)).collect();

    let mut group = c.benchmark_group("sort");

    group.bench_with_input(BenchmarkId::new("bubble", ""), &test_items,
        |b, test_items| b.iter(|| bubble_sort(&mut test_items.clone())));
    group.bench_with_input(BenchmarkId::new("insertion", ""), &test_items,
        |b, test_items| b.iter(|| insertion_sort(&mut test_items.clone())));
    group.finish();
}


criterion_group!(benches, sorting_benchmarks);
criterion_main!(benches);

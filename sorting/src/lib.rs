pub fn bubble_sort<T: Clone + PartialOrd>(items: &mut Vec<T>) {
    let mut traversal_changed_nothing = false;
    while !traversal_changed_nothing {
        traversal_changed_nothing = true;
        for i in 0..items.len() - 1 {
            if items[i] > items[i + 1] {
                let tmp = items[i].clone();
                items[i] = items[i + 1].clone();
                items[i + 1] = tmp;

                traversal_changed_nothing = false;
            }
        }
    }
}

pub fn insertion_sort<T: Clone + PartialOrd>(items: &mut Vec<T>) {
    for i in 1..items.len() {
        if items[i - 1] > items[i] {
            for j in (1..=i).rev() {
                if items[j - 1] > items[j] {
                    let tmp = items[j].clone();
                    items[j] = items[j - 1].clone();
                    items[j - 1] = tmp;
                } else {
                    break;
                }
            }
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn in_order() {
        let test_items_sorted = vec![-1, 0, 1, 2, 3, 4];
        let mut test_items_0 = test_items_sorted.clone();
        let mut test_items_1 = test_items_sorted.clone();
        bubble_sort(&mut test_items_0);
        assert_eq!(test_items_0, test_items_sorted);
        insertion_sort(&mut test_items_1);
        assert_eq!(test_items_1, test_items_sorted);
    }

    #[test]
    fn scrambled() {
        let test_items = vec![4, -1, 2, 0, 3, 1];
        let test_items_sorted: Vec<i32> = (-1..=4).collect();
        let mut test_items_0 = test_items.clone();
        let mut test_items_1 = test_items.clone();

        assert_ne!(test_items_0, test_items_sorted);
        bubble_sort(&mut test_items_0);
        assert_eq!(test_items_0, test_items_sorted);

        assert_ne!(test_items_1, test_items_sorted);
        insertion_sort(&mut test_items_1);
        assert_eq!(test_items_1, test_items_sorted);
    }
}

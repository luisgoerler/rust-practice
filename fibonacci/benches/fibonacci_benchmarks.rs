use criterion::{criterion_group, criterion_main, Criterion, BenchmarkId};
use fibonacci::{calc_fibonacci_iterative, calc_fibonacci_recursive, MemoizationEnclosure};


fn fibonacci_benchmarks(c: &mut Criterion) {
    let mut group = c.benchmark_group("fibonacci");

    group.bench_function(BenchmarkId::new("iterative", ""), |b| b.iter(|| calc_fibonacci_iterative(25)));
    group.bench_function(BenchmarkId::new("recursive", ""), |b| b.iter(|| calc_fibonacci_recursive(25)));
    group.bench_function(BenchmarkId::new("recursive with memoization", ""), |b| b.iter(|| MemoizationEnclosure::new().calc_fibonacci_recursive(25)));
    group.finish();
}


criterion_group!(benches, fibonacci_benchmarks);
criterion_main!(benches);

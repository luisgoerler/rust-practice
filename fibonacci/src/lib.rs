use std::collections::HashMap;


pub fn calc_fibonacci_iterative(n: u32) -> u32 {
    if n == 0 {
        return 0;
    }

    let mut a = 0;
    let mut b = 1;
    for _i in 0..n-1 {
        let tmp = a;
        a = b;
        b = tmp + b;
    }

    b
}

pub fn calc_fibonacci_recursive(n: u32) -> u32 {
    if n == 1 || n == 0 {
        n
    } else {
        calc_fibonacci_recursive(n - 1) + calc_fibonacci_recursive(n - 2)
    }
}

pub struct MemoizationEnclosure {
    mem: HashMap<u32, u32>,
}

impl MemoizationEnclosure {

    pub fn new() -> MemoizationEnclosure {
        MemoizationEnclosure {
            mem: HashMap::new(),
        }
    }

    pub fn calc_fibonacci_recursive(&mut self, n: u32) -> u32 {
        if n == 1 || n == 0 {
            return n;
        }

        if let Some(result) = self.mem.get(&n) {
            *result
        } else {
            let result = self.calc_fibonacci_recursive(n - 1) + self.calc_fibonacci_recursive(n - 2);
            self.mem.insert(n, result);
            result
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_numbers() -> [u32; 10] {
        [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
    }

    #[test]
    fn iterative() {
        let numbers = get_test_numbers();
        for i in 0..numbers.len() {
            assert_eq!(calc_fibonacci_iterative(i as u32), numbers[i]);
        }
    }

    #[test]
    fn recursive() {
        let numbers = get_test_numbers();
        for i in 0..numbers.len() {
            assert_eq!(calc_fibonacci_recursive(i as u32), numbers[i]);
        }
    }

    #[test]
    fn recursive_with_memoization() {
        let numbers = get_test_numbers();
        let mut mem_handler = MemoizationEnclosure::new();
        for i in 0..numbers.len() {
            assert_eq!(mem_handler.calc_fibonacci_recursive(i as u32), numbers[i]);
        }
    }
}
